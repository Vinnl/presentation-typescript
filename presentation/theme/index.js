import createTheme from "../../createTheme";

const colors = {
  primary: "hsl(0, 0%, 21%)",
  secondary: "white",
  tertiary: "hsl(171, 100%, 41%)",
  quaternary: "white" // pale gray
};

const theme = createTheme(colors, {
    primary: "Ubuntu",
    secondary: "Ubuntu"
  }, {
    progress: {
      pacmanTop: {
        background: colors.quaternary
      },
      pacmanBottom: {
        background: colors.quaternary
      },
      point: {
        borderColor: colors.quaternary
      }
    },
    components: {
      heading: {
        h1: {
          fontSize: '5rem',
          textTransform: 'uppercase'
        },
        h2: {
          fontSize: '3rem',
          textTransform: 'none'
        },
        h3: {
          fontSize: '2.5rem',
          textTransform: 'none'
        },
        h4: {
          fontSize: '2rem',
          textTransform: 'none'
        },
        h5: {
          fontSize: '1.5rem',
          textTransform: 'none'
        },
        h6: {
          fontSize: '1rem',
          textTransform: 'none'
        }
      },
      codePane: {
        fontSize: '2rem'
      }
    }
  });

export default theme;